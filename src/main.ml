open Core_kernel.Std

type pair = string * float

let pcs = [|"C"; "Db"; "D"; "Eb"; "E"; "F"; "Gb"; "G"; "Ab"; "A"; "Bb"; "B"|]

let log2 x =
  Float.log10 x /. Float.log10 2.0

let pitch_of_freq f =
  let midi = int_of_float (Float.round (69.0 +. 12.0 *. log2 (f /. 440.0))) in
  let pc = pcs.(midi mod 12) in
  let octave = midi / 12 - 1 in
  pc ^ string_of_int octave

exception Fatal of string
let fatal msg =
  raise (Fatal msg)

let input_pair lines =
  Option.map (lines ()) (fun line ->
    match String.split ~on:'\t' line with
    | [freq_s; amp_s] -> begin
        match float_of_string_opt freq_s, float_of_string_opt amp_s with
        | Some freq, Some amp -> (freq, amp)
        | _, _ -> fatal ("bad format: '" ^ line ^ "'")
      end
    | ls -> fatal ("bad format: '" ^ line ^ "' has " ^ (List.length ls |> string_of_int)))

let two_lines a b =
  let aa = snd a in
  let bb = snd b in
  if aa > bb then
    [a]
  else if bb > aa then
    [b]
  else
    [a; b]

let boundary_line a b acc =
  let aa = snd a in
  let bb = snd b in
  if aa >= bb then
    a :: acc
  else
    acc

let middle_line acc a b c =
  let ba = snd b in
  if ba > snd a && ba > snd c then
    b :: acc
  else
    acc

let find_peaks lines =
  let rec loop acc lines prev curr =
    match input_pair lines with
    | None -> boundary_line curr prev acc
    | Some pair -> loop (middle_line acc prev curr pair) lines curr pair
  in
  (match lines () with
   | Some _ -> () (* Skip header line *)
   | None -> fatal "Empty file");
  match input_pair lines with
  | None -> fatal "Empty file"
  | Some first ->
    match input_pair lines with
    | None -> [first]
    | Some second ->
      match input_pair lines with
      | None -> two_lines first second
      | Some third -> loop (boundary_line first second []) lines second third

let filter_peaks n threshold peaks =
  let rec take n acc = function
    | x :: xs when n > 0 ->
      if snd x >= threshold then
        take (n - 1) (x :: acc) xs
      else
        take n acc xs
    | _ -> List.rev acc
  in
  List.stable_sort (fun (_, a) (_, b) -> compare b a) peaks
  |> take n []

let write_words : (string -> unit) -> string array -> unit  =
  fun f ->
    Array.iter ~f

let output_peaks buffer pairs =
  let f (f, a) =
    write_words buffer [|
      "<tr><td>";
      pitch_of_freq f;
      "</td><td>";
      string_of_float a;
      "</td></tr>\n"
    |]
  in
  buffer "<table>\n";
  List.iter ~f pairs;
  buffer "</table>\n"

let run lines buffer n threshold =
  find_peaks lines
  |> filter_peaks n threshold
  |> output_peaks buffer

let line_reader string =
  let lines = ref (String.split_lines string) in
  fun () ->
    match !lines with
    | [] -> None
    | h :: t -> begin
      lines := t;
      Some h
    end

let js_obj = object%js
  method peaks content n threshold =
    let lines = line_reader (Js.to_string content) in
    let buffer = Buffer.create 64 in
    run lines (Buffer.add_string buffer) n threshold;
    Js.string (Buffer.contents buffer)
end

let () =
  Js.export "peaks" js_obj
