all: jsoo
	sed -e '/%%SCRIPT%%/{r _build/src/main.js' -e 'd}' src/peaks.html > /tmp/peaks1.html
	sed -e '/%%NORMALIZE%%/{r /home/raphael/misc/Skeleton/css/normalize.css' -e 'd}' /tmp/peaks1.html > /tmp/peaks2.html
	sed -e '/%%SKELETON%%/{r /home/raphael/misc/Skeleton/css/skeleton.css' -e 'd}' /tmp/peaks2.html > peaks.html

jsoo: src/main.ml
	ocamlbuild -use-ocamlfind -plugin-tag 'package(js_of_ocaml.ocamlbuild)' -package batteries -package 'js_of_ocaml.weak' -package js_of_ocaml.ppx src/main.js

clean:
	rm -rf ./_build peaks.html
